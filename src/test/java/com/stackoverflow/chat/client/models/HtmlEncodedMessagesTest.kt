package com.stackoverflow.chat.client.models

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.stackoverflow.chat.client.models.serializers.BulkChatEventDeserializer
import me.wietlol.serialization.JacksonSerializerAdapter
import me.wietlol.serialization.JsonSerializer
import me.wietlol.serialization.deserialize
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class HtmlEncodedMessagesTest
{
	@Test
	@DisplayName("Should return the correct message")
	fun shouldReturnCorrectMessage()
	{
		val serializer = buildSerializer()
		val json = "{\"event_type\":1,\"time_stamp\":1570895379,\"content\":\"is \\u0026quot;init\\u0026quot; the constructor on Kotlin?\",\"id\":97566785,\"user_id\":9997227,\"user_name\":\"Mehdi B.\",\"room_id\":15,\"room_name\":\"Android\",\"message_id\":47558234}"
		val message = serializer.deserialize<MessagePosted>(json)
		
		Assertions.assertThat(message.content)
			.isEqualTo("is \"init\" the constructor on Kotlin?")
	}
	
	private fun buildSerializer(): JsonSerializer =
		ObjectMapper()
			.also { it.propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE }
			.also { it.registerModule(KotlinModule()) }
			.also { mapper ->
				mapper.registerModule(
					SimpleModule()
						.apply { addDeserializer(BulkChatEvent::class.java, BulkChatEventDeserializer(mapper)) }
				)
			}
			.let { JacksonSerializerAdapter(it) }
}
