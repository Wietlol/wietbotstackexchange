package me.wietlol.serialization

interface JsonSerializer
{
	fun serialize(entity: Any?): String
	
	fun <T> deserialize(json: String, type: Class<T>): T
}

inline fun <reified T> JsonSerializer.deserialize(json: String): T =
	deserialize(json, T::class.java)
