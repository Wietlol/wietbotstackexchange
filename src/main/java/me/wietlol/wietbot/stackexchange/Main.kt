package me.wietlol.wietbot.stackexchange

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.stackoverflow.chat.client.CommonSeChatEvents
import com.stackoverflow.chat.client.HttpSeChatClientFactory
import com.stackoverflow.chat.client.SeChatClientFactory
import com.stackoverflow.chat.client.SeChatEvents
import com.stackoverflow.chat.client.models.BulkChatEvent
import com.stackoverflow.chat.client.models.serializers.BulkChatEventDeserializer
import com.stackoverflow.chat.client.websockets.LoggingWebSocketListener
import com.stackoverflow.chat.client.websockets.NoOpWebSocketListener
import com.stackoverflow.chat.client.websockets.SeChatWebSocketListener
import com.stackoverflow.chat.client.websockets.WebSocketListener
import me.wietlol.loggo.api.Logger
import me.wietlol.loggo.api.LoggerFactory
import me.wietlol.loggo.common.CommonLog
import me.wietlol.loggo.common.LogSource
import me.wietlol.loggo.common.ScopedSequenceLogger
import me.wietlol.loggo.common.ScopedSourceLogger
import me.wietlol.loggo.common.error
import me.wietlol.loggo.common.information
import me.wietlol.loggo.common.trace
import me.wietlol.loggo.core.CachingLoggerFactory
import me.wietlol.loggo.core.ConsoleLogger
import me.wietlol.loggo.core.ConverterLogger
import me.wietlol.loggo.core.FileLogger
import me.wietlol.loggo.core.FilteredLogger
import me.wietlol.loggo.core.GenericLoggerFactory
import me.wietlol.loggo.core.MultiLogger
import me.wietlol.loggo.core.levelTriggeredFilter
import me.wietlol.serialization.JacksonSerializerAdapter
import me.wietlol.serialization.JsonSerializer
import org.koin.core.context.startKoin
import org.koin.dsl.module
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

fun main(args: Array<String>)
{
	val emailAddress = args.getOrNull(0) ?: run {
		println("enter email address")
		readLine()!!
	}
	val password = args.getOrNull(1) ?: run {
		println("enter password")
		readLine()!!
	}

	val mainModule = module {
		single { CommonSeChatEvents() as SeChatEvents }
		single { buildSerializer() }
		single { buildLoggerFactory(get()) }
		factory { (get<LoggerFactory<CommonLog>>() as CachingLoggerFactory).getLogger() }
		factory { buildWebSocketListener(get(), get(), get()) }
		single { HttpSeChatClientFactory(get(), get(), get()) as SeChatClientFactory }
		single { get<SeChatClientFactory>().create(emailAddress, password) }
	}

	startKoin {
		modules(mainModule)
	}

	Wietbot().start()
}

private fun buildWebSocketListener(loggerFactory: LoggerFactory<CommonLog>, serializer: JsonSerializer, chatEvents: SeChatEvents): WebSocketListener =
	NoOpWebSocketListener()
		.let { SeChatWebSocketListener(it, serializer, chatEvents.eventMap) }
		.let { LoggingWebSocketListener(loggerFactory, it) }

private fun buildSerializer(): JsonSerializer =
	ObjectMapper()
		.also { it.registerModule(KotlinModule()) }
		.also { mapper ->
			mapper.registerModule(
				SimpleModule()
					.apply { addDeserializer(BulkChatEvent::class.java, BulkChatEventDeserializer(mapper)) }
			)
		}
		.let { JacksonSerializerAdapter(it) }

private fun buildLoggerFactory(serializer: JsonSerializer): LoggerFactory<CommonLog> =
	GenericLoggerFactory { buildLogger(serializer) }
		.let { CachingLoggerFactory(it) }

private fun buildLogger(serializer: JsonSerializer): Logger<CommonLog>
{
	val formatter = serializer::serialize
	
	val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
	val fileProvider: () -> File = { File("logs/log-${LocalDate.now().format(dateFormat)}.log.json") }
	
	val sequenceId = UUID.randomUUID()
	
	return MultiLogger(listOf<Logger<String>>(
		ConsoleLogger(),
		FileLogger(fileProvider)
	))
		.let { ConverterLogger(it, formatter) }
		.let { ScopedSourceLogger(it) { LogSource("Wietbot Server") } }
		.let { ScopedSequenceLogger(it) { sequenceId } }
		.let { FilteredLogger(it, levelTriggeredFilter(information, error, trace)) }
}
