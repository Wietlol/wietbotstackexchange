package me.wietlol.wietbot.stackexchange

import com.stackoverflow.chat.client.SeChatClient
import me.wietlol.loggo.api.Logger
import me.wietlol.loggo.common.CommonLog
import me.wietlol.loggo.common.EventId
import me.wietlol.loggo.common.logError
import me.wietlol.wietbot.stackexchange.botfeatures.BotFeature
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.reflections.Reflections

class Wietbot : KoinComponent
{
	private val worldExplosionEventId = EventId(285307088, "something went horribly wrong")
	
	private val client by inject<SeChatClient>()
	private val logger by inject<Logger<CommonLog>>()
	
	fun start()
	{
		try
		{
			registerBotFeatures()
			
			client.sendMessage(1, "Hello, World!")
		}
		catch (ex: Exception)
		{
			logger.logError(worldExplosionEventId, mapOf("shit" to "happened"), ex)
		}
	}
	
	private fun registerBotFeatures()
	{
		val koinRoot = getKoin().rootScope
		
		Reflections("me.wietlol.wietbot")
			.getTypesAnnotatedWith(BotFeature::class.java)
			.asSequence()
			.map { it.constructors.single() }
			.map { constructor -> constructor.newInstance(
				*constructor
					.parameters
					.map { koinRoot.get<Any>(it.type, null, null) }
					.toTypedArray()
			) }
			.forEach { println(it.javaClass.name) }
	}
}
