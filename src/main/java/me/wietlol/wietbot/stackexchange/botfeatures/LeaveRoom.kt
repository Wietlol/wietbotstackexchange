package me.wietlol.wietbot.stackexchange.botfeatures

import com.stackoverflow.chat.client.SeChatClient
import com.stackoverflow.chat.client.SeChatEvents

@BotFeature("leaveRoom")
class LeaveRoom(
	client: SeChatClient,
	chatEvents: SeChatEvents
)
{
	private val regex = "^@Wietbot leave (\\d+)\$".toRegex()
	
	init
	{
		(chatEvents.onMessagePosted).register {
			val match = regex.matchEntire(it.content)
			
			if (match != null)
			{
				val room = match.groups[1]!!.value.toInt()
				
				println("left room $room")
				client.leaveRoom(room)
			}
		}
	}
}
