package me.wietlol.wietbot.stackexchange.botfeatures

import com.stackoverflow.chat.client.SeChatClient
import com.stackoverflow.chat.client.SeChatEvents
import com.stackoverflow.chat.client.models.MessagePosted
import me.wietlol.wietbot.stackexchange.temporary.Constants

@BotFeature("shutdown")
class Shutdown(
	client: SeChatClient,
	chatEvents: SeChatEvents
)
{
	private val regex = "^@Wietbot shut ?down$".toRegex()
	
	init
	{
		(chatEvents.onMessagePosted).register {
			val match = regex.matchEntire(it.content)
			
			if (match != null)
			{
				if (it.isFromWietlol())
				{
					println("shutting down")
					client.sendMessage(it.roomId, "Bye, World!")
					client.leaveAllRooms()
					client.close()
				}
				else
				{
					client.sendMessage(it.roomId, "You are not allowed to use this command.")
				}
			}
		}
	}
	
	private fun MessagePosted.isFromWietlol(): Boolean =
		userId == Constants.wietlolUserId
}
