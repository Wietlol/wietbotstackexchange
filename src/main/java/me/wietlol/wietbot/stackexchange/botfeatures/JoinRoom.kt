package me.wietlol.wietbot.stackexchange.botfeatures

import com.stackoverflow.chat.client.SeChatClient
import com.stackoverflow.chat.client.SeChatEvents
import me.wietlol.loggo.api.Logger
import me.wietlol.loggo.common.CommonLog
import me.wietlol.loggo.common.EventId
import me.wietlol.loggo.common.logTrace

@BotFeature("joinRoom")
class JoinRoom(
	client: SeChatClient,
	chatEvents: SeChatEvents,
	logger: Logger<CommonLog>
)
{
	private val regex = "^@Wietbot join (\\d+)\$".toRegex()
	
	private val joinRoomEventId = EventId(282133803, "joinRoom")
	
	init
	{
		(chatEvents.onMessagePosted).register {
			val match = regex.matchEntire(it.content)
			
			if (match != null)
			{
				val room = match.groups[1]!!.value.toInt()
				
				logger.logTrace(joinRoomEventId, mapOf("room" to room))
				client.joinRoom(room)
			}
		}
	}
}
