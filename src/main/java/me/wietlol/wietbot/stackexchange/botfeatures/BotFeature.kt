package me.wietlol.wietbot.stackexchange.botfeatures

annotation class BotFeature(
	val name: String
)
