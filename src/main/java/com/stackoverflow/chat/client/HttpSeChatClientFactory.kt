package com.stackoverflow.chat.client

import com.stackoverflow.chat.client.websockets.WebSocketListener
import khttp.responses.Response
import me.wietlol.serialization.JsonSerializer

class HttpSeChatClientFactory(
	val listener: WebSocketListener,
	val chatEvents: SeChatEvents,
	val serializer: JsonSerializer
) : SeChatClientFactory
{
	override fun create(emailAddress: String, password: String): HttpSeChatClient
	{
		val cookieJar = mutableMapOf<String, String>()
		
		val mainFKey = getMainFKey(cookieJar)
		
		login(mainFKey, emailAddress, password, cookieJar)
		
		val accountFKey = getAccountFKey(cookieJar)
		
		return HttpSeChatClient(chatSiteUrl, accountFKey, cookieJar, listener, chatEvents, serializer)
	}
	
	companion object
	{
		private const val mainSiteUrl = "https://stackoverflow.com"
		internal const val chatSiteUrl = "https://chat.stackoverflow.com"
		
		private fun getMainFKey(cookieJar: MutableMap<String, String>): String =
			khttp.get("$mainSiteUrl/users/login", cookies = cookieJar)
				.apply { cookieJar.putAll(cookies) }
				.run { text.extractFKey() }
		
		private fun login(clientFkey: String, emailAddress: String, password: String, cookieJar: MutableMap<String, String>): Response =
			khttp.post(
				"$mainSiteUrl/users/login",
				data = mapOf(
					"fkey" to clientFkey,
					"email" to emailAddress,
					"password" to password
				),
				headers = mapOf("Content-Type" to "application/x-www-form-urlencoded"),
				cookies = cookieJar
			)
				.apply { cookieJar.putAll(cookies) }
				.apply {
					if (text.contains("The email or password is incorrect."))
						throw UnsuccessfulAuthenticationException("The email or password is incorrect.")
				}
				.apply {
					if (text.contains("<title>Human verification - Stack Overflow</title>"))
						throw LackOfHumanityException("Human verification is required.")
				}
				.apply {
					if (cookies.containsKey("uauth").not())
						throw UnexpectedSituationException("?????")
				}
		
		private fun getAccountFKey(cookieJar: Map<String, String>): String =
			khttp.get(chatSiteUrl, cookies = cookieJar)
				.run { text.extractFKey() }
	}
}
