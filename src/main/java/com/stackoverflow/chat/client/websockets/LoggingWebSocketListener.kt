package com.stackoverflow.chat.client.websockets

import me.wietlol.loggo.api.Logger
import me.wietlol.loggo.api.LoggerFactory
import me.wietlol.loggo.common.CommonLog
import me.wietlol.loggo.common.EventId
import me.wietlol.loggo.common.logDebug
import me.wietlol.loggo.common.logError
import me.wietlol.loggo.common.logInformation

class LoggingWebSocketListener(
	val loggerFactory: LoggerFactory<CommonLog>,
	val listener: WebSocketListener
) : WebSocketListener
{
	private val onCloseInputEventId = EventId(1688208196, "onClose-input")
	private val onCloseErrorEventId = EventId(1932693557, "onClose-error")
	override fun onClose(code: Int, reason: String, remote: Boolean)
	{
		loggerFactory.createLogger().use { logger ->
			logger.logDebug(onCloseInputEventId, mapOf("code" to code, "reason" to reason, "remote" to remote))
			logger.whileLoggingExceptions(onCloseErrorEventId) {
				listener.onClose(code, reason, remote)
			}
		}
	}
	
	private val onMessageInputEventId = EventId(2131159002, "onMessage-input")
	private val onMessageErrorEventId = EventId(54006414, "onMessage-error")
	override fun onMessage(message: String)
	{
		loggerFactory.createLogger().use { logger ->
			logger.logInformation(onMessageInputEventId, mapOf("message" to message))
			logger.whileLoggingExceptions(onMessageErrorEventId) {
				listener.onMessage(message)
			}
		}
	}
	
	private fun Logger<CommonLog>.whileLoggingExceptions(eventId: EventId, action: () -> Unit)
	{
		runCatching(action)
			.onFailure { logError(eventId, emptyMap<String, Any>(), it) }
			.getOrThrow()
	}
}
