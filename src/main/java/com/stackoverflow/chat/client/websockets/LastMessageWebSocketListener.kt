package com.stackoverflow.chat.client.websockets

class LastMessageWebSocketListener(
	val listener: WebSocketListener
) : WebSocketListener by listener
{
	lateinit var lastMessage: String
	
	override fun onMessage(message: String)
	{
		lastMessage = message
		listener.onMessage(message)
	}
}
