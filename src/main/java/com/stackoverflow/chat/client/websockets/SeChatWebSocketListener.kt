package com.stackoverflow.chat.client.websockets

import com.stackoverflow.chat.client.models.BulkChatEvent
import com.stackoverflow.chat.client.models.EventMap
import me.wietlol.serialization.JsonSerializer
import me.wietlol.serialization.deserialize

class SeChatWebSocketListener(
	val webSocketListener: WebSocketListener,
	val serializer: JsonSerializer,
	val eventMap: EventMap
) : WebSocketListener by webSocketListener
{
	override fun onMessage(message: String)
	{
		val bulkChatEvent = serializer.deserialize<BulkChatEvent>(message)
		
		bulkChatEvent
			.events
			.forEach { eventMap.get(it.javaClass)?.execute(it) }
		
		webSocketListener.onMessage(message)
	}
}
