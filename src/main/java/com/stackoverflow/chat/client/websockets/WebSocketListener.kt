package com.stackoverflow.chat.client.websockets

interface WebSocketListener
{
	fun onMessage(message: String)
	
	fun onClose(code: Int, reason: String, remote: Boolean)
}
