package com.stackoverflow.chat.client

import com.stackoverflow.chat.client.models.EventMap
import com.stackoverflow.chat.client.models.MessageDeleted
import com.stackoverflow.chat.client.models.MessageEdited
import com.stackoverflow.chat.client.models.MessageMovedIn
import com.stackoverflow.chat.client.models.MessageMovedOut
import com.stackoverflow.chat.client.models.MessagePosted
import com.stackoverflow.chat.client.models.MessageStarred
import com.stackoverflow.chat.client.models.ReplyPosted
import com.stackoverflow.chat.client.models.UserJoined
import com.stackoverflow.chat.client.models.UserLeft
import com.stackoverflow.chat.client.models.UserMentioned
import me.wietlol.reactor.ExecutableEvent

interface SeChatEvents
{
	val onMessageDeleted: ExecutableEvent<MessageDeleted>
	val onMessageEdited: ExecutableEvent<MessageEdited>
	val onMessageMovedIn: ExecutableEvent<MessageMovedIn>
	val onMessageMovedOut: ExecutableEvent<MessageMovedOut>
	val onMessagePosted: ExecutableEvent<MessagePosted>
	val onMessageStarred: ExecutableEvent<MessageStarred>
	val onReplyPosted: ExecutableEvent<ReplyPosted>
	val onUserJoined: ExecutableEvent<UserJoined>
	val onUserLeft: ExecutableEvent<UserLeft>
	val onUserMentioned: ExecutableEvent<UserMentioned>
	
	val eventMap: EventMap
}
