package com.stackoverflow.chat.client

interface SeChatClientFactory
{
	fun create(emailAddress: String, password: String): SeChatClient
}
