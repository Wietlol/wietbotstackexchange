package com.stackoverflow.chat.client

import com.stackoverflow.chat.client.models.EventMap
import com.stackoverflow.chat.client.models.MessageDeleted
import com.stackoverflow.chat.client.models.MessageEdited
import com.stackoverflow.chat.client.models.MessageMovedIn
import com.stackoverflow.chat.client.models.MessageMovedOut
import com.stackoverflow.chat.client.models.MessagePosted
import com.stackoverflow.chat.client.models.MessageStarred
import com.stackoverflow.chat.client.models.ReplyPosted
import com.stackoverflow.chat.client.models.SeChatEvent
import com.stackoverflow.chat.client.models.UserJoined
import com.stackoverflow.chat.client.models.UserLeft
import com.stackoverflow.chat.client.models.UserMentioned
import me.wietlol.reactor.Event
import me.wietlol.reactor.filteredBy

class Room(
	private val client: HttpSeChatClient,
	private val seEvents: SeChatEvents,
	val roomId: Int
)
{
	val eventMap = EventMap()
	
	val onMessageDeleted: Event<MessageDeleted>
		get() = getEvent()
	val onMessageEdited: Event<MessageEdited>
		get() = getEvent()
	val onMessageMovedIn: Event<MessageMovedIn>
		get() = getEvent()
	val onMessageMovedOut: Event<MessageMovedOut>
		get() = getEvent()
	val onMessagePosted: Event<MessagePosted>
		get() = getEvent()
	val onMessageStarred: Event<MessageStarred>
		get() = getEvent()
	val onReplyPosted: Event<ReplyPosted>
		get() = getEvent()
	val onUserJoined: Event<UserJoined>
		get() = getEvent()
	val onUserLeft: Event<UserLeft>
		get() = getEvent()
	val onUserMentioned: Event<UserMentioned>
		get() = getEvent()
	
	fun leave() = client.leaveRoom(roomId)
	
	fun sendMessage(text: String) = client.sendMessage(roomId, text)
	
	fun editMessage(messageId: Int, text: String) = client.editMessage(messageId, text)
	
	private inline fun <reified E : SeChatEvent> getEvent(): Event<E> =
		eventMap.computeIfAbsent { seEvents.eventMap.get<E>()!!.filteredBy(::isInRoom) }
	
	private fun isInRoom(event: SeChatEvent) =
		event.roomId == roomId
}
