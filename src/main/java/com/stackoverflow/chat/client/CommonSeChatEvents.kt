package com.stackoverflow.chat.client

import com.stackoverflow.chat.client.models.EventMap
import com.stackoverflow.chat.client.models.MessageDeleted
import com.stackoverflow.chat.client.models.MessageEdited
import com.stackoverflow.chat.client.models.MessageMovedIn
import com.stackoverflow.chat.client.models.MessageMovedOut
import com.stackoverflow.chat.client.models.MessagePosted
import com.stackoverflow.chat.client.models.MessageStarred
import com.stackoverflow.chat.client.models.ReplyPosted
import com.stackoverflow.chat.client.models.UserJoined
import com.stackoverflow.chat.client.models.UserLeft
import com.stackoverflow.chat.client.models.UserMentioned
import me.wietlol.reactor.ExecutableEvent
import me.wietlol.reactor.asSwallowed

class CommonSeChatEvents : SeChatEvents
{
	override val onMessageDeleted: ExecutableEvent<MessageDeleted> = ExecutableEvent.create<MessageDeleted>().asSwallowed()
	override val onMessageEdited: ExecutableEvent<MessageEdited> = ExecutableEvent.create<MessageEdited>().asSwallowed()
	override val onMessageMovedIn: ExecutableEvent<MessageMovedIn> = ExecutableEvent.create<MessageMovedIn>().asSwallowed()
	override val onMessageMovedOut: ExecutableEvent<MessageMovedOut> = ExecutableEvent.create<MessageMovedOut>().asSwallowed()
	override val onMessagePosted: ExecutableEvent<MessagePosted> = ExecutableEvent.create<MessagePosted>().asSwallowed()
	override val onMessageStarred: ExecutableEvent<MessageStarred> = ExecutableEvent.create<MessageStarred>().asSwallowed()
	override val onReplyPosted: ExecutableEvent<ReplyPosted> = ExecutableEvent.create<ReplyPosted>().asSwallowed()
	override val onUserJoined: ExecutableEvent<UserJoined> = ExecutableEvent.create<UserJoined>().asSwallowed()
	override val onUserLeft: ExecutableEvent<UserLeft> = ExecutableEvent.create<UserLeft>().asSwallowed()
	override val onUserMentioned: ExecutableEvent<UserMentioned> = ExecutableEvent.create<UserMentioned>().asSwallowed()
	
	override val eventMap = EventMap()
		.also { it.add(onMessageDeleted) }
		.also { it.add(onMessageEdited) }
		.also { it.add(onMessageMovedIn) }
		.also { it.add(onMessageMovedOut) }
		.also { it.add(onMessagePosted) }
		.also { it.add(onMessageStarred) }
		.also { it.add(onReplyPosted) }
		.also { it.add(onUserJoined) }
		.also { it.add(onUserLeft) }
		.also { it.add(onUserMentioned) }
}
