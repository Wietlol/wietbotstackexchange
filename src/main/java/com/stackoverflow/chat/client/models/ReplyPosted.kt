package com.stackoverflow.chat.client.models

data class ReplyPosted(
	override val eventType: EventType,
	val timeStamp: Int,
	override val content: String,
	override val id: Int,
	val userId: Int,
	val targetUserId: Int,
	val userName: String,
	override val roomId: Int,
	val roomName: String,
	val messageId: Int,
	val parentId: Int,
	val showParent: Boolean
) : MessageEvent
