package com.stackoverflow.chat.client.models

interface MessageEvent : SeChatEvent
{
	val content: String
}
