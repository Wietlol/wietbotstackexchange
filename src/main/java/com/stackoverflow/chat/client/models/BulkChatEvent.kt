package com.stackoverflow.chat.client.models

data class BulkChatEvent(
	val events: List<SeChatEvent>
)
