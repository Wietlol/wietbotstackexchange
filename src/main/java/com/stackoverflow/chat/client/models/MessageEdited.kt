package com.stackoverflow.chat.client.models

data class MessageEdited(
	override val eventType: EventType,
	val timeStamp: Int,
	override val content: String,
	override val id: Int,
	val userId: Int,
	val userName: String,
	override val roomId: Int,
	val roomName: String,
	val messageId: Int,
	val messageEdits: Int
) : MessageEvent
