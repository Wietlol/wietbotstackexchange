package com.stackoverflow.chat.client.models

data class MessageMovedOut(
	override val eventType: EventType,
	override val roomId: Int,
	override val id: Int
) : SeChatEvent
