package com.stackoverflow.chat.client.models

data class MessageMovedIn(
	override val eventType: EventType,
	override val roomId: Int,
	override val id: Int
) : SeChatEvent
