package com.stackoverflow.chat.client.models

interface SeChatEvent
{
	val id: Int
	val eventType: EventType
	val roomId: Int
}
