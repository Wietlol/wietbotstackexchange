package com.stackoverflow.chat.client.models

data class MessageStarred(
	override val eventType: EventType,
	val timeStamp: Int,
	override val content: String,
	override val id: Int,
	override val roomId: Int,
	val roomName: String,
	val messageId: Int,
	val messageStars: Int
) : MessageEvent
