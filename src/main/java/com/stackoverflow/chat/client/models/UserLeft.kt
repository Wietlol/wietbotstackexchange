package com.stackoverflow.chat.client.models

data class UserLeft(
	override val eventType: EventType,
	val timeStamp: Int,
	override val id: Int,
	val userId: Int,
	val targetUserId: Int,
	val userName: String,
	override val roomId: Int,
	val roomName: String
) : SeChatEvent
