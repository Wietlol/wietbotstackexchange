package com.stackoverflow.chat.client

import java.io.Closeable

interface SeChatClient : Closeable
{
	fun reconnect(roomId: Int)
	
	fun joinRoom(roomId: Int): Room
	
	fun getRoom(roomId: Int): Room
	
	fun leaveRoom(roomId: Int)
	
	fun leaveAllRooms()
	
	fun sendMessage(roomId: Int, text: String)
	
	fun editMessage(messageId: Int, text: String)
}
