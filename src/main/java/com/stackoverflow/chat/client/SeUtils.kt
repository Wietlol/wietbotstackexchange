package com.stackoverflow.chat.client

fun String.extractFKey(): String =
	sequenceOf(
		"<input type=\"hidden\" name=\"fkey\" value=\"",
		"<input id=\"fkey\" name=\"fkey\" type=\"hidden\" value=\""
	)
		.map {
			val head = indexOf(it)
			if (head > 0)
			{
				val start = head + it.length
				val end = indexOf("\"", start)
				substring(start, end)
			}
			else
				null
		}
		.filterNotNull()
		.firstOrNull()
		?: throw UnexpectedSituationException("there is no fkey?")
