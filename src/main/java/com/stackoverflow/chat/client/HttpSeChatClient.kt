package com.stackoverflow.chat.client

import com.stackoverflow.chat.client.websockets.JavaWebSocketClientAdapter
import com.stackoverflow.chat.client.websockets.ReconnectingWebSocketListener
import com.stackoverflow.chat.client.websockets.WebSocketListener
import me.wietlol.serialization.JsonSerializer
import me.wietlol.serialization.deserialize
import org.java_websocket.client.WebSocketClient
import java.io.Closeable
import java.net.URI
import java.time.Instant
import java.util.*

class HttpSeChatClient(
	private val chatSiteUrl: String,
	private val accountFKey: String,
	private val cookieJar: Map<String, String>,
	private val listener: WebSocketListener,
	private val chatEvents: SeChatEvents,
	private val serializer: JsonSerializer,
	private val initialRoom: Int = 1
) : SeChatClient, Closeable
{
	private var client: WebSocketClient = connect(initialRoom)
	
	private val rooms: MutableMap<Int, Room> = HashMap()
	
	private var isClosing = false
	
	override fun reconnect(roomId: Int)
	{
		runCatching {
			client.close()
		}
		
		client = connect(roomId)
	}
	
	private fun reconnectIfNotClosing()
	{
		if (isClosing.not())
			reconnect(initialRoom)
	}
	
	override fun joinRoom(roomId: Int): Room =
		khttp.get("$chatSiteUrl/rooms/$roomId")
			.apply {
				if (statusCode == 404)
					throw RoomNotFoundException("Room $roomId does not exist.")
			}
			.apply { text.extractFKey() }
			.run { getRoom(roomId) }
			.also { reconnect(it.roomId) }
	
	override fun getRoom(roomId: Int): Room =
		rooms.computeIfAbsent(roomId) { Room(this, chatEvents, it) }
	
	override fun leaveRoom(roomId: Int)
	{
		khttp.post(
			"$chatSiteUrl/chats/leave/$roomId",
			data = mapOf(
				"quiet" to "true",
				"fkey" to accountFKey
			),
			cookies = cookieJar
		)
	}
	
	override fun leaveAllRooms()
	{
		khttp.post(
			"$chatSiteUrl/chats/leave/all",
			data = mapOf(
				"quiet" to "true",
				"fkey" to accountFKey
			),
			cookies = cookieJar
		)
	}
	
	override fun sendMessage(roomId: Int, text: String)
	{
		khttp.post(
			"$chatSiteUrl/chats/$roomId/messages/new",
			data = mapOf(
				"text" to text,
				"fkey" to accountFKey
			),
			cookies = cookieJar
		)
	}
	
	override fun editMessage(messageId: Int, text: String)
	{
		khttp.post(
			"$chatSiteUrl/messages/$messageId",
			data = mapOf(
				"text" to text,
				"fkey" to accountFKey
			),
			cookies = cookieJar
		)
	}
	
	override fun close()
	{
		isClosing = true
		
		client.close()
	}
	
	
	private data class WsAuthResponse(val url: String)
	private fun connect(roomId: Int): WebSocketClient
	{
		val wssResponse: WsAuthResponse = khttp.post(
			"$chatSiteUrl/ws-auth",
			cookies = cookieJar,
			data = mapOf(
				"roomId" to roomId.toString(),
				"fkey" to accountFKey
			),
			headers = mapOf(
				"Origin" to chatSiteUrl,
				"Referer" to "$chatSiteUrl/rooms/$roomId",
				"Content-Type" to "application/x-www-form-urlencoded"
			)
		)
			.let { serializer.deserialize(it.text) }
		
		val chatSiteUrl = "https://chat.stackoverflow.com"
		val webSocketUrl = "${wssResponse.url}?l=${Instant.now().toEpochMilli()}"
		
		val listener = listener
			.let { // todo find out how to pass this via DI
				ReconnectingWebSocketListener(it) {
					reconnectIfNotClosing()
				}
			}
		
		return JavaWebSocketClientAdapter(URI(webSocketUrl), mapOf("Origin" to chatSiteUrl), listener)
			.apply { connect() }
	}
}
